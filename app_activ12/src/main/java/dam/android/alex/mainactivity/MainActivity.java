package dam.android.alex.mainactivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {
    private final String MYPREFS="MyPrefs";
    private String priority="Normal";
    private EditText etPlayerName;
    private Spinner spinnerLevel;
    private Spinner spinnerColor;
    private EditText etScore;
    private Button btQuit;
    private CheckBox checkSound;
    private RadioGroup radioGroup;
    private RadioGroup radioGroupPreferences;
    private ConstraintLayout constraintLayout;


    @SuppressLint("ResourceType")
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();


    }
    private  void setUI(){

        etPlayerName=findViewById(R.id.etPlayerName);
        spinnerLevel =findViewById(R.id.spinnerLevel);
        spinnerColor=findViewById(R.id.spinnerColor);
        checkSound=findViewById(R.id.checkSound);
        radioGroup=(RadioGroup)findViewById(R.id.radioGroup);
        constraintLayout=(ConstraintLayout)findViewById(R.id.constraintLayout);
        radioGroup.check(R.id.radioButtonNormal);
        radioGroupPreferences=findViewById(R.id.radioGroup2);






        ArrayAdapter<CharSequence> spinnerAdapter=ArrayAdapter.createFromResource(this,R.array.levels, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(spinnerAdapter);


        ArrayAdapter<CharSequence> adapterSpinner=ArrayAdapter.createFromResource(this,R.array.background, android.R.layout.simple_spinner_dropdown_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerColor.setAdapter(adapterSpinner);

        radioGroup.setOnCheckedChangeListener(this);


        changeBackgroudColor();


        etScore =findViewById(R.id.etScore);
        btQuit=findViewById(R.id.btPreferences);
        btQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Todo Ponemos la musica cuando cambiamos de Activity
                putMusic();
                //TODO Se crea un Intent Explicito  el cual se comunica con la siguiente actividad el cual le pasamas la id de radioGrup que se ha seleccionado
                Intent intent=new Intent(getApplicationContext(),NewActivity.class);
                intent.putExtra("info",radioGroupPreferences.getCheckedRadioButtonId());
                // Todo Inicia el intent
                startActivity(intent);


                finish();
            }
        });


    }

    @Override
    protected void onPause(){
        super.onPause();
        SharedPreferences myPreferences=getSharedPreferences(MYPREFS,MODE_PRIVATE);
        //SharedPreferences myPreferences=getPreferences(MODE_PRIVATE);
        //SharedPreferences myPreferences= PreferenceManager.getDefaultSharedPreferences(this);

        SharedPreferences.Editor editor= myPreferences.edit();

        editor.putString("PlayerName",etPlayerName.getText().toString());
        editor.putInt("Level",spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score",Integer.parseInt(etScore.getText().toString()));
        editor.putInt("Color",spinnerColor.getSelectedItemPosition());
        editor.putInt("RadioGroup",radioGroup.getCheckedRadioButtonId());
        editor.putBoolean("Sound",checkSound.isChecked());


        editor.apply();

    }

    @Override
    protected void onResume(){
        super.onResume();

        SharedPreferences myPreferences=getSharedPreferences(MYPREFS,MODE_PRIVATE);


        etPlayerName.setText(myPreferences.getString("PlayerName","unknown"));
        spinnerLevel.setSelection(myPreferences.getInt("Level",0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score",0)));
        spinnerColor.setSelection(myPreferences.getInt("Color",0));
        checkSound.setChecked(myPreferences.getBoolean("Sound",false));
        radioGroup.check(myPreferences.getInt("RadioGroup",0));




    }
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public void comentarParte2(){
        //TODO 2.1.1  SharedPreferences myPreferences = getPreferences(MODE_PRIVATE);
        // Lo que hace es que desde una acividad ,puedes acceder a las preferencias de esa actividad y
        // lo guarda en el archivo de preferencias de esa actividad. Se guardaría el nombre  de la activity

        //TODO 2.1.2 SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        // Lo que hace es que guarda los ajustes generales en el fichero de preferencia de esta aplicacion.
        // Utiliza un valor predeterminado de preferencia el nombre del archivo. Este valor predeterminado se establece para cada aplicación, de modo que todas las actividades en el mismo contexto de la aplicación puede acceder a ella fácilmente
        // Se guarda la con el nombre del pakage_preferences.xml


        //
    }









    public void changeBackgroudColor(){

        //TODO Ponermos un listener en el spinner para que cuando cambiamos el item que esta seleccionado
        // lo que hace es que pilla el color que tenemos en el arrays de color y se lo pone al background
        spinnerColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Resources rs=getResources();

                String[] Colors=rs.getStringArray(R.array.backgroundColor);
                constraintLayout.setBackgroundColor(Color.parseColor(Colors[spinnerColor.getSelectedItemPosition()]));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }



    public void putMusic(){
        //TODO Lo que hace es que pone la musica solamente cuando se selecciona el checkBox de la musica
        MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.musica);
        if (checkSound.isChecked()){
            mediaPlayer.start();
        }

    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        // Todo lo que hacemos es que le marcamos la prioridad segun seleccione en el RadioGroup
        switch (checkedId){
            case R.id.radioButtonEasy:
                priority="Easy";
                break;
            case R.id.radioButtonNormal:
                priority="Normal";
                break;
            case R.id.radioButtonHard:
                priority="Hard";
                break;
            case R.id.radioButtonVeryHard:
                priority="Very Hard";
                break;
            case R.id.radioButtonExpert:
                priority="Expert";
                break;

        }

    }






}