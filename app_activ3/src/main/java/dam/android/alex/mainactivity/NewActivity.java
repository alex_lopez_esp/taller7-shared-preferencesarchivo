package dam.android.alex.mainactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class NewActivity extends AppCompatActivity {
    private final String MYPREFS="MyPrefs";
    private SharedPreferences myPreferences;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        setUI();
    }

    @SuppressLint({"ResourceType", "SetTextI18n"})
    private void setUI() {
        this.textView=findViewById(R.id.textView);
        Bundle bundle=getIntent().getExtras();



        int prueba=bundle.getInt("info");
        System.out.println(prueba);
        //TODO Creo un switch con la id del radioGroup para obtener el que se ha pulsado para que me realice diferentes archivos
        //TODO Me almacena la informacion  y se la introduce en el textView
        switch (prueba){
            case R.id.rbgetSharedPreferences:
                //TODO Me almacena la informacion  y se la introduce en el textView
                myPreferences=getSharedPreferences(MYPREFS,MODE_PRIVATE);
                preferences("MyPrefs");

                break;

            case R.id.rbgetPreferences:
                myPreferences=getPreferences(MODE_PRIVATE);
                preferences("MainActivity.xml");



                break;

            case R.id.rbGetDefaultSharedPreferences:
                myPreferences= PreferenceManager.getDefaultSharedPreferences(this);
                preferences("dam.android.alex.mainactivity_preferences.xml");



                break;

        }

    }
    @SuppressLint("SetTextI18n")
    public void preferences(String preferences){
        //TODO PONER TODA LA INFORMACION QUE SE PASA DE UNA CLASE A LA OTRA
        textView.setText("SHOWING SHARED PREFS FILE:\n"+preferences+"\n\nPLAYER: "+myPreferences.getString("PlayerName","unknown")
                +"\n"+"SCORE: " +myPreferences.getInt("Score",0)
                +"\n"+"LEVEL: "+myPreferences.getInt("Level",0)
                +"\n"+"DIFFICULTY: "+myPreferences.getInt( "RadioGroup",0)
                +"\n"+"SOUND: "+myPreferences.getBoolean("Sound",false)
                +"\n"+"BG COLOR: "+myPreferences.getInt("Color",0));

    }
}